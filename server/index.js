const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const port = 3000;

//middleware

app.use(bodyParser.json());
app.use(cors());
app.options("*", cors());

//routes defining
app.get("/", (req, res) => {
  res.json({
    message: " The game will be starting in 20 mins"
  });
});
app.get("/data", (req, res) => {
  res.json({
    stats: [
      { id: 1, name: "Manchester City", city: "coventry", cups: 45 },
      { id: 2, name: "Leicester", city: "coventry", cups: 3 },
      { id: 3, name: "Manchester United", city: "coventry", cups: 5 }
    ]
  });
});

app.post("/data", (req, res) => {
  res.json(req.body);
});
app.listen(port, () => {
  console.log("we are up and running..");
});
